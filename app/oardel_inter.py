"""
oardel_inter:

Interactive Oardel
"""
import subprocess as sp
import json

def get_oarstat_json(user=""):
    """
    returns the json output of the
    `oarstat -J -u` command
    """
    json_str = sp.check_output(f"oarstat -J -u {user}", shell=True)
    if len(json_str) == 0:
        return {}
    json_content = json.loads(json_str)
    return json_content

def get_user_oar_job_ids(oarstat_json):
    """
    returns the list of OAR_JOB_IDs of
    the user's running jobs
    """
    return [job_id for job_id, job_info in oarstat_json.items() if job_info.get("state") == "Running"]

def get_job_id_to_kill(job_ids_list):
    """
    returns the oar job id of the job to kill
    """
    print("# Current Running OAR Jobs")
    for job_id_index, job_id in enumerate(job_ids_list):
        print(f"  [{job_id_index + 1}] {job_id}")
    try:
        user_choice_str = input("> Job to kill: ")
        user_choice = int(user_choice_str) - 1
        assert 0 <= user_choice < len(job_ids_list)
    except KeyboardInterrupt:
        return None
    except ValueError:
        print("Enter a number")
    except AssertionError:
        print(f"Enter a valid index (between 1 and {len(job_ids_list)} included)")
    else:
        return job_ids_list[user_choice]
    return None

def kill_job(job_id):
    """
    kill the oar job with this id
    """
    try:
        sp.check_call(f"oardel {job_id}", shell=True)
    except sp.CalledProcessError:
        print("oardel exited with non zero code")
        return 1
    else:
        print("Job killed succesfully")
        return 0

def main():
    """
    main function
    """
    oarstat_json = get_oarstat_json()
    oar_job_ids = get_user_oar_job_ids(oarstat_json)
    if len(oar_job_ids) == 0:
        print("No job to kill")
        return 0
    job_id_to_kill = get_job_id_to_kill(oar_job_ids)
    if job_id_to_kill is None:
        return 1
    return kill_job(job_id_to_kill)

if __name__ == "__main__":
    main()
